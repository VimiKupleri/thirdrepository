

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchWithCss {

	public static void main(String[] args) {
			
		System.setProperty("webdriver.chrome.driver", "C:\\VDFData\\Eclips\\chromedriver_win32\\chromedriver.exe" );
		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElement(By.cssSelector("#consent_prompt_submit")).click();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Jeans");
		driver.findElement(By.cssSelector("button.searchButton")).click();
		//driver.findElements(By.cssSelector("ln-c-form-option.ln-c-form-option--checkbox.ln-c-form-option--facet")).get(162).click();
		//WebDriverWait wait = new WebDriverWait(driver, 15);
		//wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='�5-�9.99-d']")));
		driver.findElement(By.cssSelector("label[for='�5-�9.99-d']")).click();
	}

}
