
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class StoreLocator {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\VDFData\\Eclips\\chromedriver_win32\\chromedriver.exe" );
		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElement(By.id("consent_prompt_submit")).click();
		driver.findElement(By.cssSelector("[href='/store-finder']")).click();
	//	driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/ul[2]/li[2]/a")).click();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		driver.findElement(By.cssSelector("[name=q]")).click();
		driver.findElement(By.cssSelector("[name=q]")).sendKeys("Rg14 5Lf");
		driver.findElements(By.cssSelector("label.ln-c-form-option__label")).get(1).click();
		driver.findElements(By.cssSelector(".ln-c-form-option__label")).get(3).click();
		driver.findElements(By.cssSelector(".ln-c-button.ln-c-button--primary")).get(2).click();
	}

}
